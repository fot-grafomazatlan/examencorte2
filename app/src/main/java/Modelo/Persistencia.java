package Modelo;


import com.example.preeexamencorte2java.Ventas;

public interface Persistencia {
    void openDataBase();
    void closeDataBase();
    long insertVenta(Ventas venta);
}
